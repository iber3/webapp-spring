package lt.iber.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lt.iber.entity.Note;
import lt.iber.service.NoteService;

@Controller
public class NoteController {
	
	@Autowired
	private NoteService noteService;
	
	
	@GetMapping("/list")
	public String listNotes(Model theModel)
	{	
		List<Note> theNotes = noteService.getNotes();
		
		theModel.addAttribute("notes", theNotes);
		return "note-list.jsp";
	}  
	
	@PostMapping("/processForm")
	public String processForm(@ModelAttribute("note") Note theNote)
	{	
		noteService.saveNote(theNote);
		return "redirect:/list";
	}  
	
	@RequestMapping("/showForm")
	public String showForm(Model theModel) {
		Note theNote = new Note();
		
		theModel.addAttribute("note", theNote);
		
		return "note-form.jsp";
	}
	
	@GetMapping("/showEditForm")
	public String showEditForm(@RequestParam("noteId") int theId, Model theModel ) {
		
		Note theNote = noteService.getNote(theId);
		
		theModel.addAttribute("note", theNote);
		
		return "note-form.jsp";
	}
	
	@GetMapping("/delete")
	public String deleteNote(@RequestParam("noteId") int theId) {
		
		noteService.deleteNote(theId);
		
		return "redirect:/list";
	}

}
