package lt.iber.service;

import java.util.List;

import lt.iber.entity.Note;

public interface NoteService {
	
	public List<Note> getNotes();
	
	public void saveNote(Note theNote);

	public Note getNote(int theId);

	public void deleteNote(int theId);

}
