package lt.iber.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lt.iber.dao.NoteDAO;
import lt.iber.entity.Note;

@Service
public class NoteServiceImpl implements NoteService {

	
	@Autowired
	private NoteDAO noteDAO;
	
	
	@Override
	@Transactional
	public List<Note> getNotes() {
		return noteDAO.getNotes();
	}


	@Override
	@Transactional
	public void saveNote(Note theNote) {
		
		noteDAO.saveNote(theNote);
		
	}


	@Override
	@Transactional
	public Note getNote(int theId) {
		// TODO Auto-generated method stub
		return noteDAO.getNote(theId);
	}


	@Override
	@Transactional
	public void deleteNote(int theId) {
		// TODO Auto-generated method stub
		noteDAO.deleteNote(theId);
	}

}
