<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Note Form</title>
<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/styles/add-note-style.css" />
</head>
<body>
<div class="form-style-5" style="text-align:center">
			<form:form action = "processForm" modelAttribute="note" method="POST">
				<form:hidden path="id" />
			<fieldset>
			<legend><span class="number">1</span> Add note</legend>
			<form:input type="text" path="title" placeholder="Title"/>
			<form:input type="text" path="text" placeholder="Text"/>
			<input type="submit" value="Submit" />
			</fieldset>
			</form:form>
			<a href="/web-app/list">Back to existing notes</a>
		</div>
</body>
</html>