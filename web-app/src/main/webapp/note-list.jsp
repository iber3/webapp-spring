<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<title> List</title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/styles/table-style.css" />
</head>
	<body>
		<div id="container">
	
		<div id="content">
		
		
			<table>
				<tr>
					<th>Id</th>
					<th>Title</th>
					<th>Text</th>
					<th>Action</th>
				</tr>
				
				<c:forEach var="note" items="${notes}">
				
				<c:url var="editLink" value = "/showEditForm">
					<c:param name ="noteId" value="${note.id }" />
				</c:url>
				
				<c:url var="deleteLink" value = "/delete">
					<c:param name ="noteId" value="${note.id }" />
				</c:url>
					<tr>
						<td> ${note.id } </td>
						<td> ${note.title} </td>
						<td> ${note.text} </td>
						<td> 
						<a href ="${editLink }">Edit</a> <a href ="${deleteLink }"
							onclick="if (!(confirm('Are you sure?'))) return false" >Delete</a>
						 </td>
					</tr>
				
				</c:forEach>
						
			</table>
			<input type="button" value ="New note"
				onclick ="window.location.href='showForm'; return false;"
				class ="add-button"
				/>
				
		</div>
	
	</div>
</body>
</html>