## Spring MVC & Hibernate Notes web application

## CRUD Operations

    Adding notes
    Updating notes
    Deleting notes

## Technologies

    Maven Project
    Apache Tomcat
    Spring Java Configuration
    Hibernate Session Factory
    Firebird SQL